﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data.Entity;
using HSGest.Entities;
using System.Data.Entity.Migrations;

namespace HSGest
{
    public class HSContext : DbContext
    {
        public HSContext() : base(nameOrConnectionString: "Default") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("public");
           
        }

        public DbSet<Cliente> Clienti;
    }
}

namespace Core.Persistence
{
    public class Configuration : DbMigrationsConfiguration<HSGest.HSContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }
}
