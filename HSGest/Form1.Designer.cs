﻿namespace HSGest
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_clienti = new System.Windows.Forms.Button();
            this.btn_riparazioni = new System.Windows.Forms.Button();
            this.btn_fornitori = new System.Windows.Forms.Button();
            this.btn_ordini = new System.Windows.Forms.Button();
            this.btn_fatture = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_clienti
            // 
            this.btn_clienti.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_clienti.Location = new System.Drawing.Point(80, 156);
            this.btn_clienti.Name = "btn_clienti";
            this.btn_clienti.Size = new System.Drawing.Size(121, 48);
            this.btn_clienti.TabIndex = 0;
            this.btn_clienti.Text = "Gestione clienti";
            this.btn_clienti.UseVisualStyleBackColor = true;
            // 
            // btn_riparazioni
            // 
            this.btn_riparazioni.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_riparazioni.Location = new System.Drawing.Point(291, 156);
            this.btn_riparazioni.Name = "btn_riparazioni";
            this.btn_riparazioni.Size = new System.Drawing.Size(121, 48);
            this.btn_riparazioni.TabIndex = 0;
            this.btn_riparazioni.Text = "Gestione riparazioni";
            this.btn_riparazioni.UseVisualStyleBackColor = true;
            // 
            // btn_fornitori
            // 
            this.btn_fornitori.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_fornitori.Location = new System.Drawing.Point(80, 246);
            this.btn_fornitori.Name = "btn_fornitori";
            this.btn_fornitori.Size = new System.Drawing.Size(121, 48);
            this.btn_fornitori.TabIndex = 0;
            this.btn_fornitori.Text = "Gestione fornitori";
            this.btn_fornitori.UseVisualStyleBackColor = true;
            // 
            // btn_ordini
            // 
            this.btn_ordini.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_ordini.Location = new System.Drawing.Point(291, 246);
            this.btn_ordini.Name = "btn_ordini";
            this.btn_ordini.Size = new System.Drawing.Size(121, 48);
            this.btn_ordini.TabIndex = 0;
            this.btn_ordini.Text = "Gestione ordini";
            this.btn_ordini.UseVisualStyleBackColor = true;
            // 
            // btn_fatture
            // 
            this.btn_fatture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_fatture.Location = new System.Drawing.Point(187, 345);
            this.btn_fatture.Name = "btn_fatture";
            this.btn_fatture.Size = new System.Drawing.Size(121, 48);
            this.btn_fatture.TabIndex = 0;
            this.btn_fatture.Text = "Gestione fatture";
            this.btn_fatture.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.modificaToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(492, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.esciToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(94, 22);
            this.esciToolStripMenuItem.Text = "Esci";
            // 
            // modificaToolStripMenuItem
            // 
            this.modificaToolStripMenuItem.Name = "modificaToolStripMenuItem";
            this.modificaToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.modificaToolStripMenuItem.Text = "Modifica";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(19)))), ((int)(((byte)(24)))));
            this.pictureBox1.Image = global::HSGest.Properties.Resources.bg_image;
            this.pictureBox1.Location = new System.Drawing.Point(141, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(211, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(19)))), ((int)(((byte)(24)))));
            this.ClientSize = new System.Drawing.Size(492, 405);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_ordini);
            this.Controls.Add(this.btn_fatture);
            this.Controls.Add(this.btn_fornitori);
            this.Controls.Add(this.btn_riparazioni);
            this.Controls.Add(this.btn_clienti);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "H&S Gest";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_clienti;
        private System.Windows.Forms.Button btn_riparazioni;
        private System.Windows.Forms.Button btn_fornitori;
        private System.Windows.Forms.Button btn_ordini;
        private System.Windows.Forms.Button btn_fatture;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

